define(function(require, exports, module) {
    "use strict";

    main.consumes = ["ui", "menus", "help", "layout", "settings", "configure", "commands", "Plugin", "smartface.about"];
    main.provides = ["smartface.theme"];
    module.exports = main;

    function main(options, imports, register) {
        var ui = imports.ui,
            staticPrefix = options.staticPrefix,
            menus = imports.menus,
            layout = imports.layout,
            settings = imports.settings,
            css_dark, css_white,
            commands = imports.commands,
            Plugin = imports.Plugin,
            help = imports.help,
            smfAbout = imports["smartface.about"];
            
        var plugin = new Plugin("Smartface", main.consumes);
        var loaded = false;
        plugin.on("load", function(e) {
            if (loaded) return false;
            loaded = true;
            commands.addCommand({
                name: "smf.Emulator",
                isAvailable: function() {
                    return true;
                },
                exec: function() {
                    alert("Emulator will feature come in future releases");
                }
            }, plugin);
            ui.insertByIndex(layout.getElement('barTools'),
                new ui.button({
                    id: "btnSmartfaceEmulator",
                    skin: "c9-toolbarbutton-glossy",
                    command: "smf.Emulator",
                    caption: "Emulator",
                    disabled: false,
                    class: "emulatorBtn",
                    icon: "run.png",
                }), 999, plugin);
        });

        plugin.freezePublicAPI({});
        plugin.on("unload", function(e) {
            loaded = false;
        })

        require('plugins/smartface.ide.theme/setFavIcon')();
        require('plugins/smartface.ide.theme/setTitle')();
        ui.insertCss(require("text!./smf.less"), staticPrefix, main);

        var smartfaceLogoOnMenu = document.createElement("img");
        smartfaceLogoOnMenu.setAttribute("src", "static/plugins/smartface.ide.theme/images/smartface_logo_white.png");
        smartfaceLogoOnMenu.setAttribute("alt", "Smartface Cloud IDE");
        smartfaceLogoOnMenu.setAttribute("style", "padding-left:3px;");
        smartfaceLogoOnMenu.onmouseup = function() {
          smfAbout.show(); 
        };
        var firstChild = menus.container.firstChild;
        menus.container.insertBefore(smartfaceLogoOnMenu, firstChild);

        var c9Menu = firstChild.nextElementSibling.nextElementSibling;
        c9Menu.textContent = "IDE";
        c9Menu.style.display = "none";
        
        var supportMenu = menus.container.lastChild;
        supportMenu.style.display = "none";

        /*var q8 = document.getElementById("q8");
        if (q8) {
            var c9MenuSubItems = [].slice.call(q8.children),
                menuItem;
            for (var i = 0; i < c9MenuSubItems.length; i++) {
                menuItem = c9MenuSubItems[i];
                switch (i) {
                    case 0:
                        menuItem.style.display = "none";
                        break;

                }
            }
        }*/

        function setTheme(e) {
            var flatLight = e.theme == "flat-light",
                logoPath;
            if (flatLight) {
                logoPath = staticPrefix + "/images/smartface_logo_white.png";
            }
            else {
                logoPath = staticPrefix + "/images/smartface_logo.png";
            }
            smartfaceLogoOnMenu.setAttribute("src", logoPath);

        }
        
        layout.on("themeChange", setTheme);
        setTheme({
            theme: settings.get("user/general/@skin")
        });

        var backToC9 = document.getElementById("backToC9");
        backToC9.target = "";
        backToC9.href = "#";
        backToC9.onclick = help.showAbout;
        register(null, {
            "smartface.theme": plugin
        });

        console.log('Smartface theme applied');





    }
    return main;
});
