define(function(require, exports, module) {
    function setFavIcon() {
        var links = document.getElementsByTagName('link'),
            link,
            rel,
            sizes,
            href,
            i;
        for (i = 0; i < links.length; i++) {
            link = links[i];
            rel = link.getAttribute("rel");
            if (rel === 'icon' || rel === 'shortcut icon') {
                sizes = link.getAttribute("sizes");
                switch (sizes) {
                    case "16x16":
                        href = 'favicon16.ico';
                        break;
                    case "32x32":
                    default:
                        href = 'favicon32.ico';
                        break;
                }
                link.setAttribute('href', 'https://az793023.vo.msecnd.net/c9-dashboard/dashboard/' + href);
            }
        }
    }
    exports = setFavIcon;
    return setFavIcon;
});